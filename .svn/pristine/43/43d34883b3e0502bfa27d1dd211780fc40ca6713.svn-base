package com.twinfeats.interfacebook.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.twinfeats.interfacebook.java.Comments;
import com.twinfeats.interfacebook.java.Photo;
import com.twinfeats.interfacebook.java.Post;
import com.twinfeats.interfacebook.java.Users;
import com.twinfeats.interfacebook.sql.SQLUtil;

/**
 * Servlet implementation class IntefacebookServlet
 */
@WebServlet(urlPatterns={"/InterfacebookServlet"}, loadOnStartup=1)
public class InterfacebookServlet extends HttpServlet {
	private static final Logger logger = LoggerFactory.getLogger(InterfacebookServlet.class);
	private static final long serialVersionUID = 1L;
    /**
     * Default constructor. 
     */
    public InterfacebookServlet() {
        // TODO Auto-generated constructor stub
    }
    
    @Override
	public void init(ServletConfig config) throws ServletException {
		try {
			//initialize the JDBC JNDI datasource named "interface". This maps to
			//config entries in Tomcat's context.xml file and this apps web.xml file.
			SQLUtil.init("interface");
		} catch (NamingException e) {
			logger.error("",e);
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String v = request.getParameter("start");
		try {
			if (v != null) {
				getPostEntries(request, response);
			}
			
			else {
				v = request.getParameter("status");
				if (v != null) {
					postIt(request, response);
				}
				else {
					v = request.getParameter("entryid");
					if (v != null) {
						//get the photos for an entry
						getPhotos(request, response);
					}
					else {
						v = request.getParameter("action");
						if (v != null) {
							//load public users
							getPublicUsers(request, response);
						}
						else {
							v=request.getParameter("action2");
							if (v != null){
								addComments(request, response);
							}
							else {
								v=request.getParameter("action3");
								if (v != null){
									loadComments(request, response);
								}
							}
						}
					}	
				}
					
			}
				
		}
		catch (IOException e) {
			e.printStackTrace();
			JSONObject json = new JSONObject();
			json.put("error", "Something went wrong! Try again later!");
			PrintWriter writer = response.getWriter();
			writer.print(json.toString());
		}
		
	}
	
	private void addComments(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String v = request.getParameter("statusID");
		int statusID = Integer.parseInt(v);
		String comment = request.getParameter("comment");
		String cemoticon = request.getParameter("cemoticon");
		Users u = (Users)request.getSession().getAttribute(Users.USER);
		String cname = request.getParameter("cname");
		try{
			Comments cmt = new Comments();
			cmt.setStatusId(statusID);
			cmt.setComment(comment);
			cmt.setCemoticon(cemoticon);
			cmt.setCname(u.getUserName());
			cmt.save();
			JSONObject json = new JSONObject();
			json.put("status", "Thanks for your comment!");
			response.setContentType("application/json");
			PrintWriter writer = response.getWriter();
			writer.write(json.toString());
		} catch (Exception e){
			logger.error("Could not save Comments",e);
			JSONObject json = new JSONObject();
			json.put("error",true);
			PrintWriter writer = response.getWriter();
			writer.write(json.toString());
		}
	}

	private void postIt(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String status = request.getParameter("status");
		String emoticon = request.getParameter("emoticon");
		String[] files = request.getParameterValues("files");
		try {
			HttpSession session = request.getSession();
			Users u = (Users)session.getAttribute(Users.USER);
			Post post = new Post();
			System.out.println("postIt function started");
			post.setName(u.getUserName());
			post.setStatus(status);
			post.setEmoticon(emoticon);
			post.setFiles(files);
			post.save();
			if (files != null) {
				//This loop is not very efficient, doing an INSERT for each Photo. This should be a SQL BATCH operation.
				for (String f:files) {
					logger.debug("Uploaded: {}",f);
					Photo photo = new Photo();
					photo.setPostId(post.getId());
					photo.setUri(f);
					photo.save();
				}
			}
			JSONObject json = new JSONObject();
			json.put("status", "Thanks for your post!");
			response.setContentType("application/json");
			PrintWriter writer = response.getWriter();
			writer.write(json.toString());
		} catch (Exception e) {
			logger.error("Could not add Interfacebook entry",e);
			JSONObject json = new JSONObject();
			json.put("error", "Could not save entry! Try again later!");
			PrintWriter writer = response.getWriter();
			writer.write(json.toString());
		}
	}

	private void getPostEntries(HttpServletRequest request, HttpServletResponse response) throws IOException {
		System.out.println("getPostEntries function started");
		String v = request.getParameter("start");
		int start = Integer.parseInt(v);
		v = request.getParameter("count");
		System.out.println(v);
		if (v != null) {
			int count = Integer.parseInt(v);
			String username = request.getParameter("username");
			System.out.println(username);
				try {
					List<Post> list = Post.load(start, count, username);
					System.out.println("Post.load(start,count, username) started");
					JSONArray jsonarray = new JSONArray();
					for (Post pst:list) {
						JSONObject json = new JSONObject();
						json.put("id", pst.getId());
						json.put("name", pst.getName());
						json.put("status", pst.getStatus());
						json.put("emoticon", pst.getEmoticon());
						json.put("files", pst.getFiles());
						json.put("username", pst.users.getUserName());
						json.put("displayName", pst.users.getDisplayName());
						json.put("count", pst.getCommentCount());
						jsonarray.put(json); 
						
					}
					response.setContentType("application/json");
					PrintWriter writer = response.getWriter();
					writer.write(jsonarray.toString());
					System.out.println("jsonarry to string");
				} catch (Exception e) {
					logger.error("Could not load Post entries",e);
					JSONObject json = new JSONObject();
					json.put("error",true);
					PrintWriter writer = response.getWriter();
					writer.write(json.toString());
				}
		}
		
	}
	
	private void loadComments(HttpServletRequest request, HttpServletResponse response) throws IOException {
		System.out.println("loadComments function started");
		String v = request.getParameter("statusID");
		int id = Integer.parseInt(v);
		System.out.println(v);
				try {
					List<Comments> list = Comments.load(id);
					System.out.println("Comments.load(statusId) started");
					JSONArray jsonarray = new JSONArray();
					for (Comments cmnt:list) {
						JSONObject json = new JSONObject();
						json.put("statusId", cmnt.getStatusId());
						json.put("comment", cmnt.getComment());
						json.put("cname", cmnt.getCname());
						json.put("cemoticon", cmnt.getCemoticon());
						json.put("count", cmnt.getCount());
						jsonarray.put(json); 
						
					}
					response.setContentType("application/json");
					PrintWriter writer = response.getWriter();
					writer.write(jsonarray.toString());
					System.out.println("jsonarry to string");
				} catch (Exception e) {
					logger.error("Could not load Comments",e);
					JSONObject json = new JSONObject();
					json.put("error",true);
					PrintWriter writer = response.getWriter();
					writer.write(json.toString());
				}
		}
		
	
	protected void getPhotos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String v = request.getParameter("entryid");
		logger.debug("Getting photos for {}",v);
		int id = Integer.parseInt(v);
		try {
			List<Photo> list = Photo.load(id);
			JSONArray jsonarray = new JSONArray();
			for (Photo p:list) {
				logger.debug("Getting Photo {}",p.getUri());
				jsonarray.put(p.getUri());
			}
			response.setContentType("application/json");
			PrintWriter writer = response.getWriter();
			writer.write(jsonarray.toString());
		} catch (Exception e) {
			logger.error("Could not load Interfacebook entries",e);
			JSONObject json = new JSONObject();
			json.put("error",true);
			PrintWriter writer = response.getWriter();
			writer.write(json.toString());
		}
	}
	
	protected void getPublicUsers(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.debug("Getting public users for {}");
		try {
			List<Users> list = Users.publicUsers();
			JSONArray jsonarray = new JSONArray();
			for (Users u:list) {
				JSONObject json = new JSONObject();
				json.put("username", u.getUserName());
				json.put("displayName", u.getDisplayName());
				jsonarray.put(json);
			}
			response.setContentType("application/json");
			PrintWriter writer = response.getWriter();
			writer.write(jsonarray.toString());
		} catch (Exception e) {
			logger.error("Could not load Public Users",e);
			JSONObject json = new JSONObject();
			json.put("error",true);
			PrintWriter writer = response.getWriter();
			writer.write(json.toString());
		}
	}
}
