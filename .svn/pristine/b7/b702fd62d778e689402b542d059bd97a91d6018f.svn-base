package com.twinfeats.interfacebook.java;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;	
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.twinfeats.interfacebook.sql.SQLUtil;
import com.twinfeats.interfacebook.web.ResultSetHandler;

public class Post {
	private int id;
	private String name;
	private Date created;
	private String status;
	private String emoticon;
	private String[] files;
	public Users users; 

	
	public String[] getFiles() {
		return files;
	}

	public void setFiles(String[] files) {
		this.files = files;
	}

	private List<Photo> photos = new LinkedList<Photo>();
	private static Logger logger = LoggerFactory.getLogger(Post.class);
	
	/**
	 * Build the next Post entry from the result set.
	 */
	private static ResultSetHandler<Post> simpleHandler = new ResultSetHandler<Post>() {
		public Post processRow(ResultSet rs) throws SQLException {
			Post gb = new Post();
			gb.setId(rs.getInt("id"));
			gb.setName(rs.getString("name"));
			gb.setStatus(rs.getString("status"));
			gb.setEmoticon(rs.getString("emoticon"));
			String[] files = new String [1];
			files [0] = rs.getString("uri");
			gb.setFiles(files);
			Users u = Users.processUsers(rs); 
			gb.users= u; 
			return gb;
		}
	};

	public void addPhoto(Photo photo) {
		photos.add(photo);
	}
	
	public List<Photo> getPhotos() {
		return photos;
	}
	
	/**
	 * Saves a Post to the DB. This method does NOT save Photos, those are done by InterfacebookServlet.
	 * @return
	 * @throws SQLException
	 */
	public int save() throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = SQLUtil.getConnection();
			String sql = "INSERT INTO Post (name, status, emoticon) VALUES (?,?,?)";
			ps = conn.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			int idx = 1;
			ps.setString(idx++,name);
			ps.setString(idx++, status);
			ps.setString(idx++, emoticon);
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs != null && rs.next()) {
			    id = rs.getInt(1);
			}
			int count = ps.getUpdateCount();
			return count;
		}
		finally {
			if (ps != null) {
				try {
					ps.close();
				}
				catch (Throwable t) {
				}
			}
			if (conn != null) {
				try {
					conn.close();
				}
				catch (Throwable t) {
				}
			}
		}
	}
	
	/**
	 * Load the next page (list) of Post entries.
	 * @param start
	 * @param count
	 * @return
	 * @throws SQLException
	 */
	public static List<Post> load(int start, int count, String username) throws SQLException {
		List<Post> list;
		StringBuilder sb = new StringBuilder(200);
		if (username == null) {
			sb.append("SELECT * from Post LEFT JOIN Photos on Post.id = Photos.postid  INNER JOIN Users on Users.username = Post.name WHERE Users.showPublic = 'Y' ORDER BY created DESC, id LIMIT ");
			sb.append(count);
			sb.append(" OFFSET ");
			sb.append(start);
			logger.info("Post select {}",sb.toString());
			list = SQLUtil.executeStatement(sb.toString(), simpleHandler);
		}
		else{
			sb.append("SELECT * from Post LEFT JOIN Photos on Post.id = Photos.postid INNER JOIN Users on Users.username = Post.name WHERE Users.username='");
			sb.append(username);
			sb.append("' ORDER BY created DESC, id LIMIT ");
			sb.append(count);
			sb.append(" OFFSET ");
			sb.append(start);
			logger.info("Post select {}",sb.toString());
			list = SQLUtil.executeStatement(sb.toString(), simpleHandler);
		}
		logger.info("Post select {}",sb.toString());
		list = SQLUtil.executeStatement(sb.toString(), simpleHandler);
		return list;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getEmoticon() {
		return emoticon;
	}

	public void setEmoticon(String emoticon) {
		this.emoticon = emoticon;
	}

}
