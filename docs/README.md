Setup
--------

1. Download tomcat 8
1. Download eclipse MARS JEE edition
1. import this project
1. Load interface2_2015-07-29.sql into mysql using http://www.sequelpro.com/ 
   a. File menu's import command
1. Copy mysql JAR to $TOMCAT_HOME/lib
1. Copy context.xml from this directory to $TOMCAT_HOME/conf
1. Launch the app from within eclipse

