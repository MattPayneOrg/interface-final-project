# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.6.23)
# Database: interface2
# Generation Time: 2015-07-29 21:00:45 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table Comments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Comments`;

CREATE TABLE `Comments` (
  `statusId` int(11) unsigned NOT NULL,
  `comment` varchar(4000) DEFAULT '',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `cname` varchar(50) NOT NULL DEFAULT '',
  `cemoticon` varchar(100) DEFAULT NULL,
  `commentId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`commentId`),
  KEY `cname` (`cname`),
  KEY `statusid` (`statusId`),
  CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`cname`) REFERENCES `Users` (`username`),
  CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`statusId`) REFERENCES `Post` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

LOCK TABLES `Comments` WRITE;
/*!40000 ALTER TABLE `Comments` DISABLE KEYS */;

INSERT INTO `Comments` (`statusId`, `comment`, `created`, `cname`, `cemoticon`, `commentId`)
VALUES
	(18,'awesome post!!!','2015-06-18 23:51:35','naomi',NULL,1),
	(18,'great post dude!!!','2015-06-18 23:52:45','naomi',NULL,2),
	(19,'Stuff and things','2015-06-20 11:46:11','naomi',NULL,4),
	(19,'Stuff and things on repeat. Stuff and things on repeat. Stuff and things on repeat. Stuff and things on repeat. Stuff and things on repeat. Stuff and things on repeat. Stuff and things on repeat. Stuff and things on repeat. Stuff and things on repeat. Stuff and things on repeat. Stuff and things on repeat. Stuff and things on repeat. Stuff and things on repeat. Stuff and things on repeat. Stuff and things on repeat. Stuff and things on repeat. Stuff and things on repeat. Stuff and things on repeat. Stuff and things on repeat. Stuff and things on repeat. Stuff and things on repeat. Stuff and things on repeat. Stuff and things on repeat. Stuff and things on repeat. Stuff and things on repeat. Stuff and things on repeat. Stuff and things on repeat. Stuff and things on repeat. ','2015-06-21 23:18:08','naomi',NULL,6),
	(22,'Hey Chad what are you doing this weekend with your family? Thank you for helping me debug my issue with my try/catch in class tonight!','2015-06-23 15:28:32','naomi',NULL,11);

/*!40000 ALTER TABLE `Comments` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table Photos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Photos`;

CREATE TABLE `Photos` (
  `uri` varchar(200) NOT NULL DEFAULT '',
  `postId` int(11) unsigned NOT NULL,
  PRIMARY KEY (`uri`),
  KEY `postid` (`postId`),
  CONSTRAINT `photos_ibfk_1` FOREIGN KEY (`postId`) REFERENCES `Post` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `Photos` WRITE;
/*!40000 ALTER TABLE `Photos` DISABLE KEYS */;

INSERT INTO `Photos` (`uri`, `postId`)
VALUES
	('/book/uploads/images/143381164050810989417_10206039108235438_6266121213630209456_o.jpg',5),
	('/book/uploads/images/143407414182510475254_10204270423539426_7968873330020347845_n.jpg',10),
	('/book/uploads/images/143407583134510394118_897888283607990_6573764319742844733_n.jpg',11),
	('/book/uploads/images/14341743878761521306_10202924717097606_1776278739_n.jpg',12),
	('/book/uploads/images/14341745100761460112_10202564035240785_1763468992_n.jpg',13),
	('/book/uploads/images/14341745690481422622_10202429274991863_1060526758_n.jpg',14),
	('/book/uploads/images/14341746075761460112_10202564035240785_1763468992_n.jpg',15),
	('/book/uploads/images/1434174701084FullSizeRender (46).jpg',16),
	('/book/uploads/images/143417473730910475254_10204270423539426_7968873330020347845_n.jpg',17),
	('/book/uploads/images/1434672013091coding.jpg',18),
	('/book/uploads/images/1434818722085codingM.jpg',19),
	('/book/uploads/images/1434820819234codingM.jpg',20),
	('/book/uploads/images/1434938896042IMG_0408.jpg',21),
	('/book/uploads/images/1434942682905seemenaomi2.jpg',22),
	('/book/uploads/images/1435087780262coding.jpg',23),
	('/book/uploads/images/1435088185658binary_code.jpg',24);

/*!40000 ALTER TABLE `Photos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table Post
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Post`;

CREATE TABLE `Post` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL DEFAULT '',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(4000) DEFAULT NULL,
  `emoticon` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  CONSTRAINT `post_ibfk_1` FOREIGN KEY (`name`) REFERENCES `Users` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

LOCK TABLES `Post` WRITE;
/*!40000 ALTER TABLE `Post` DISABLE KEYS */;

INSERT INTO `Post` (`id`, `name`, `created`, `status`, `emoticon`)
VALUES
	(5,'naomi','2015-06-09 08:53:21','stuff and things and stuff and ramblings to take up space and more ramblings and oh boy! Let\'s talk about talking who doesn\'t love talking? And rambling on and on about talking to fill up space on a page to see how things react to that let\'s do that! yeah!','emoticons/Cool.png'),
	(10,'heather','2015-06-11 20:55:45','Stuff and things and stuff and stuff and omgoodness wowowowowowowowowowowowowowowowowow !','emoticons/Nerd.png'),
	(11,'chad','2015-06-11 21:23:58','Words blurbs yerd curds merds words blerds nerds stuff and things and stuff and words and so many words and so many many words all the words.','emoticons/Cool.png'),
	(12,'chad','2015-06-13 00:46:31','Lorem ipsum dolor sit amet, ex virtute salutandi sed, mel error errem comprehensam at. Id sit consequat consectetuer, ex aeterno tamquam appetere duo. Probatus rationibus no eam. Ex mutat quaestio vim. Cibo nostro expetenda vix ne, ea partem tibique accusamus mel, nam et stet probo minim.\n\nAn viderer legimus albucius mel. Nostro quodsi alterum vis ex, duis erat his te. Duo autem melius audiam an, vix in nonumy iracundia, vis ad mundi verear laoreet. Nam id dicunt eirmod scaevola, eum impetus nusquam te, summo aliquid oportere ex sea. Cu feugiat impedit partiendo sit, ad ridens docendi pericula duo.\n\nMel cu periculis mnesarchum vituperatoribus. Nibh nominavi ei has, minim partem expetendis et quo. Mel quodsi efficiantur et, mel at ludus copiosae contentiones, his te eros errem facilis. Qui ne bonorum philosophia, qui ut agam dicunt sententiae, cu semper pertinacia interesset his. Aliquam elaboraret percipitur quo at, qui at tritani integre apeirian.\n\nEu usu ferri iudico. Nisl placerat et cum. Cu qui inermis epicurei voluptatibus. Modo accommodare nec ad, diceret suscipit definitiones mei ex. Idque impetus no mei, cum ridens nusquam ne. Malis vitae civibus est et, eos et sonet omnes voluptatum.','emoticons/Cool.png'),
	(13,'chad','2015-06-13 00:48:43','orem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.','emoticons/Sick.png'),
	(14,'naomi','2015-06-13 00:49:33','orem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.','emoticons/Surprised.png'),
	(15,'naomi','2015-06-13 00:50:10','t is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).','emoticons/Cool.png'),
	(16,'naomi','2015-06-13 00:51:44','Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.','emoticons/Happy.png'),
	(17,'naomi','2015-06-13 00:52:20','There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.','emoticons/Nerd.png'),
	(18,'naomi','2015-06-18 19:00:19','egrdgadfhgfda','emoticons/Nerd.png'),
	(19,'naomi','2015-06-20 11:45:54','Hello how are you?','emoticons/Nerd.png'),
	(20,'naomi','2015-06-20 12:20:28','GSGASDGFDAGDFAGDSFJGKDSJFGKAFJGKFDJGKSDJGKDSJGSDKGJSDKGJSDKGJSDKFGJSDKFJDSKFJDSKFJSDKFJSDKFJSDKFJSDKFJSDKFJSDKFJSDKFJSDKFJSDKFJSDKFJSDKFJSDKFJSDKFJSKFJSDKFJSDKFJSK','emoticons/Cool.png'),
	(21,'chad','2015-06-21 21:08:21','sfsdgsfgsgsgdsgsfgsfgsdfdfasfasfsdgsdfadfasfadsfasdfasdfasdfs','emoticons/Cool.png'),
	(22,'chad','2015-06-21 22:11:26','Test driving new posts','emoticons/Nerd.png'),
	(23,'shonna','2015-06-23 14:29:52','Today I taught a bunch of young girls how to code! It was awesome!','emoticons/Happy.png'),
	(24,'kent','2015-06-23 14:36:32','i love my family, Java, and Kayaking ! ','emoticons/Nerd.png');

/*!40000 ALTER TABLE `Post` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table UserRole
# ------------------------------------------------------------

DROP TABLE IF EXISTS `UserRole`;

CREATE TABLE `UserRole` (
  `role` varchar(20) NOT NULL DEFAULT '',
  `username` varchar(40) NOT NULL DEFAULT '',
  PRIMARY KEY (`role`,`username`),
  KEY `userRole_fk_1` (`username`),
  CONSTRAINT `userRole_fk_1` FOREIGN KEY (`username`) REFERENCES `Users` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `UserRole` WRITE;
/*!40000 ALTER TABLE `UserRole` DISABLE KEYS */;

INSERT INTO `UserRole` (`role`, `username`)
VALUES
	('Member','chad'),
	('Member','courtney'),
	('Member','dani'),
	('Member','heather'),
	('Member','josh'),
	('Member','kent'),
	('Member','naomi'),
	('Member','shonna');

/*!40000 ALTER TABLE `UserRole` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table Users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Users`;

CREATE TABLE `Users` (
  `username` varchar(40) NOT NULL DEFAULT '',
  `password` varchar(50) NOT NULL DEFAULT '',
  `showPublic` char(1) NOT NULL DEFAULT 'Y',
  `email` varchar(50) NOT NULL DEFAULT '',
  `displayName` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `Users` WRITE;
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;

INSERT INTO `Users` (`username`, `password`, `showPublic`, `email`, `displayName`)
VALUES
	('chad','goodgood','Y','chad@see.net','interfaceChad'),
	('courtney','goodgood','Y','courtney@see.net','interfaceCourtney'),
	('dani','goodgood','Y','dani@see.net','interfaceDani'),
	('heather','goodgood','Y','heather@see.net','interfaceHeather'),
	('josh','goodgood','Y','josh@see.net','interfaceJosh'),
	('kent','goodgood','Y','kent@see.net','interfaceKent'),
	('naomi','goodgood','Y','naomi.see@seenaomi.net','interfaceNaomi'),
	('shonna','goodgood','Y','shonna@see.net','interfaceShonna');

/*!40000 ALTER TABLE `Users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
