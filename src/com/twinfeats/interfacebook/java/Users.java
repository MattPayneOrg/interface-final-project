package com.twinfeats.interfacebook.java;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.twinfeats.interfacebook.sql.SQLUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.twinfeats.interfacebook.web.ResultSetHandler;

public class Users {
	public static final String USER = "User";
	private String userName;
	private String password;
	private String showPublic; //this is a CHAR in SQL. Will this have issues converting to String?
	private String email;
	private String displayName;
	
	
	private static Logger logger = LoggerFactory.getLogger(Users.class);
	
	public static Users loadUser(String userName) throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			conn = SQLUtil.getConnection();
			String sql = "SELECT * FROM Users WHERE userName = ?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, userName);
			rs = ps.executeQuery();
			Users users = new Users();
			if (rs.next()) {
				String username = rs.getString("username");
				users.setUserName(username);
				String showPublic = rs.getString("showPublic");
				users.setShowPublic(showPublic);
				String email = rs.getString("email");
				users.setEmail(email);
				String displayName = rs.getString("displayName");
				users.setDisplayName(displayName);
				return users;
			}
		}
		finally {
			if (rs != null) {
				try {
					rs.close();
				}
				catch (Throwable t) {
				}
			}
			if (ps != null) {
				try {
					ps.close();
				}
				catch (Throwable t) {
				}
			}
			if (conn != null) {
				try {
					conn.close();
				}
				catch (Throwable t) {
				}
			}
		}
		return null;
		
	}
	
	private static ResultSetHandler<Users> usersHandler = new ResultSetHandler<Users>() {
		public Users processRow(ResultSet rs) throws SQLException {
			Users u = new Users();
			u.setUserName(rs.getString("username"));
			u.setDisplayName(rs.getString("displayName"));
			return u;
		}
	};
	
	public static Users processUsers(ResultSet rs) throws SQLException {
		Users u = new Users();
		u.setUserName(rs.getString("username")); 
		u.setPassword(rs.getString("password")); 
		u.setShowPublic(rs.getString("showPublic")); 
		u.setEmail(rs.getString("email")); 
		u.setDisplayName(rs.getString("displayName")); 
		return u;
	}
	
	public static List<Users> publicUsers() throws SQLException {
		List<Users> list;
		StringBuilder sb = new StringBuilder(200);
			sb.append("SELECT * from Users WHERE showPublic = 'Y'");
			logger.info("Users select {}",sb.toString());
			list = SQLUtil.executeStatement(sb.toString(), usersHandler);
			return list; 
		}
	
	
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getShowPublic() {
		return showPublic;
	}
	public void setShowPublic(String showPublic) {
		this.showPublic = showPublic;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	
}
