package com.twinfeats.interfacebook.java;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.twinfeats.interfacebook.sql.SQLUtil;
import com.twinfeats.interfacebook.web.ResultSetHandler;

public class Photo {
	private int postId;
	private String uri;
	private static Logger logger = LoggerFactory.getLogger(Photo.class);
	private static ResultSetHandler<Photo> photoHandler = new ResultSetHandler<Photo>() {
		public Photo processRow(ResultSet rs) throws SQLException {
			Photo p = new Photo();
			p.setPostId(rs.getInt("postId"));
			p.setUri(rs.getString("uri"));
			return p;
		}
	};

	public static List<Photo> load(int id) throws SQLException {
		List<Photo> list;
		StringBuilder sb = new StringBuilder(200);
		sb.append("SELECT * from Photos WHERE postId=");
		sb.append(id);
		logger.info("Photo select {}",sb.toString());
		list = SQLUtil.executeStatement(sb.toString(), photoHandler);
		return list;
	}
	

	public int save() throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = SQLUtil.getConnection();
			String sql = "INSERT INTO Photos (postId, uri) VALUES (?,?)";
			ps = conn.prepareStatement(sql);
			int idx = 1;
			ps.setInt(idx++,postId);
			ps.setString(idx++, uri);
			ps.execute();
			int count = ps.getUpdateCount();
			return count;
		}
		finally {
			if (ps != null) {
				try {
					ps.close();
				}
				catch (Throwable t) {
				}
			}
			if (conn != null) {
				try {
					conn.close();
				}
				catch (Throwable t) {
				}
			}
		}
	}
	
	public int getPostId() {
		return postId;
	}
	public void setPostId(int postId) {
		this.postId = postId;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
}
