package com.twinfeats.interfacebook.web;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface ResultSetHandler<T> {
	public T processRow(ResultSet rs) throws SQLException;
}

