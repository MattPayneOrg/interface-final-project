<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Interfacebook Login Page</title>
<!-- <link rel="stylesheet" href="stylesheet01.css" type="text/css"/> -->
<script src="jquery-2.1.3.min.js"></script>
<link href='http://fonts.googleapis.com/css?family=Architects+Daughter'
	rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="stylesheet01.css" type="text/css"/>
<title>Login</title>
<script type="text/javascript">
    function setFocus() {
    	document.getElementById("name").focus();
    }
</script>
<style>
body {
background-image: url('images/background2.jpg');
background-repeat: no-repeat;
background-position: center top;
}

#signon {
	border-radius: 5px;
	padding: 3px;
	width: 80%;	
}

td:nth-child(odd) {
	text-align: right; 
	}
input {
font-family: 'Architects Daughter', cursive;
	font-size: .8em; 
}	
</style>
</head>

<body id="nosize" onload="setFocus()" style="text-align: center">
<div id= "signonheader"> <img id="logo" src= "images/interfacebooklogo.png"/></div>
<form id= "signon" action="j_security_check" method="POST" style="width: 400px; display: inline-block">
<table>
<caption>Login</caption>
	<tr>
		<td>User:</td>
		<td><input id="name" type="text" name="j_username" /></td>
	</tr>
	<tr>
		<td>Password:</td>
		<td><input type="password" name="j_password" /></td>
	</tr>

	<tr>
		<td colspan="2" style="text-align: center"><input type="submit" name="submit"
			value="Sign-in" /></td>
	</tr>
</table>
</form>
</body>
</html>